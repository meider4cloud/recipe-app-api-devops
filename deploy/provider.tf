terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0" // 2.54.0
    }
  }
  backend "s3" {
    bucket         = "marcus-eder-udemy-terraform-state"
    key            = "recipe/recipe-app.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region = "eu-central-1"

}


