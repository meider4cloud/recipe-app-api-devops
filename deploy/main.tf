locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Contact     = var.contact
    ManagedBy   = "Terraform"
    Udemy       = "DevOps Deployment Automation with Terraform, AWS and Docker"
  }
}

data "aws_region" "current" {

}
